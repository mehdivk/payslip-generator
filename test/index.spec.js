var assert           = require('assert')
var PayslipGenerator = require('../')
var payslipGenerator = null

describe('PayslipGenerator', function() {
  var taxTable = null;

  beforeEach(function() {
    taxTable = require('./fixture/tax-table')
  })

  describe('new PayslipGenerator()', function() {
    it('should complain when taxTable is not passed', function() {
      assert.throws(function() {
        new PayslipGenerator()
      })
    })

    it('should complain when first param(taxTable) is not array', function() {
      assert.throws(function() {
        new PayslipGenerator('i_am_not_array')
      })
    })

    it('should complain when argument is an empty array', function() {
      assert.throws(function() {
        new PayslipGenerator([])
      })
    })

    it('should complain when argument items do not match expected format', function() {
      assert.throws(function() {
        new PayslipGenerator([{foo: 'bar'}])
      })
    })

    it('should not complain when passed param is valid', function() {
      assert.doesNotThrow(function() {
        new PayslipGenerator(taxTable)
      })
    })

  })

  describe('PayslipGenerator.prototype.generate()', function() {
    it('should return an error when firstName is missing', function() {
      var expected = [new Error('firstName is missing')]
      var actual   = new PayslipGenerator(taxTable).generate()
      assert.deepEqual(actual, expected)
    })

    it('should return an error when lastName is missing', function() {
      var expected = [new Error('lastName is missing')]
      var actual   = new PayslipGenerator(taxTable).generate('Mehdi')
      assert.deepEqual(actual, expected)
    })

    it('should return an error when annualSalary is missing', function() {
      var expected = [new Error('annualSalary is missing')]
      var actual   = new PayslipGenerator(taxTable).generate('Mehdi', 'Valikhani')
      assert.deepEqual(actual, expected)
    })

    it('should return an error when superRate is missing', function() {
      var expected = [new Error('superRate is missing')]
      var actual   = new PayslipGenerator(taxTable).generate('Mehdi', 'Valikhani', 60500)
      assert.deepEqual(actual, expected)
    })

    it('should return an error when paymentPeriod is missing', function() {
      var expected = [new Error('paymentPeriod is missing')]
      var actual   = new PayslipGenerator(taxTable).generate('Mehdi', 'Valikhani', 60500, 9)
      assert.deepEqual(actual, expected)
    })

    it('should return an error when annualSalary is invalid', function() {
      var expected = [new Error('annualSalary is invalid')]
      var actual   = new PayslipGenerator(taxTable).generate('Mehdi', 'Valikhani', 'a', 9, 'March')
      assert.deepEqual(actual, expected)
    })

    it('should return an error when annualSalary is 0', function() {
      var expected = [new Error('annualSalary is invalid')]
      var actual   = new PayslipGenerator(taxTable).generate('Mehdi', 'Valikhani', 0, 9, 'March')
      assert.deepEqual(actual, expected)
    })

    it('should return an error when annualSalary is less than 0', function() {
      var expected = [new Error('annualSalary is invalid')]
      var actual   = new PayslipGenerator(taxTable).generate('Mehdi', 'Valikhani', -10, 9, 'March')
      assert.deepEqual(actual, expected)
    })

    it('should return an error when superRate is invalid', function() {
      var expected = [new Error('superRate is invalid')]
      var actual   = new PayslipGenerator(taxTable).generate('Mehdi', 'Valikhani', 60500, 'a', 'March')
      assert.deepEqual(actual, expected)
    })

    it('should return an error when superRate is less than 0', function() {
      var expected = [new Error('superRate is invalid')]
      var actual   = new PayslipGenerator(taxTable).generate('Mehdi', 'Valikhani', 60500, -1, 'March')
      assert.deepEqual(actual, expected)
    })

    it('should return an error when superRate is greater or equal to 100', function() {
      var expected = [new Error('superRate is invalid')]
      var actual   = new PayslipGenerator(taxTable).generate('Mehdi', 'Valikhani', 60500, 100, 'March')
      assert.deepEqual(actual, expected)
    })

    it('should return an error when tax rate is not available', function() {
      var firstName     = 'David'
      var lastName      = 'Rudd'
      var annualSalary  = 182000
      var superRate     = 9
      var paymentPeriod = 'March'

      var expected = [new Error('Could not find tax rates')]
      var actual   = new PayslipGenerator(taxTable).generate(firstName, lastName, annualSalary, superRate, paymentPeriod)
      assert.deepEqual(actual, expected)
    })

    it('should generate payslip object', function() {
      var firstName     = 'David'
      var lastName      = 'Rudd'
      var annualSalary  = 60050
      var superRate     = 9
      var paymentPeriod = 'March'

      var expected = {
        firstName      : 'David',
        lastName       : 'Rudd',
        paymentPeriod  : '01 March - 31 March',
        grossIncome    : 5004,
        incomeTax      : 922,
        netIncome      : 4082,
        superAnnuation : 450
      }

      var actual = new PayslipGenerator(taxTable).generate(firstName, lastName, annualSalary, superRate, paymentPeriod)

      assert.deepEqual(actual, [null, expected])
    })
  })
})
