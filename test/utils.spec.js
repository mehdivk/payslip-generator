var assert = require('assert')
var utils  = require('../utils')

describe('utils', function() {
  describe('isValidNumber()', function() {
    it('should return false when value is null', function() {
      assert.equal(utils.isValidNumber(null), false)
    })

    it('should return false when value is undefined', function() {
      assert.equal(utils.isValidNumber(undefined), false)
    })

    it('should return false when value is string', function() {
      assert.equal(utils.isValidNumber('12'), false)
    })

    it('should return false when value is less than 0', function() {
      assert.equal(utils.isValidNumber(-1), false)
    })

    it('should return true when value is 0', function() {
      assert.equal(utils.isValidNumber(0), true)
    })

    it('should return true when value is greater than 0', function() {
      assert.equal(utils.isValidNumber(0), true)
    })
  })

  describe('.isValidTaxTableItem()', function() {
    var taxItem = null

    beforeEach(function() {
      taxItem = {
        minSalary    : 18201,
        maxSalary    : 37000,
        baseTax      : 0,
        taxPerDollar : 0.19
      }
    })

    it('should return false when minSalary is missing', function() {
      delete taxItem.minSalary
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when minSalary is not number', function() {
      taxItem.minSalary = 'a'
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when minSalary is less than 0', function() {
      taxItem.minSalary = -1
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when maxSalary is missing', function() {
      delete taxItem.maxSalary
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when maxSalary is not number', function() {
      taxItem.maxSalary = 'a'
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when maxSalary is less than 0', function() {
      taxItem.maxSalary = -1
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when baseTax is missing', function() {
      delete taxItem.baseTax
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when baseTax is not number', function() {
      taxItem.baseTax = 'a'
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when baseTax is less than 0', function() {
      taxItem.baseTax = -1
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when taxPerDollar is missing', function() {
      delete taxItem.taxPerDollar
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when taxPerDollar is not number', function() {
      taxItem.taxPerDollar = 'a'
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when taxPerDollar is less than 0', function() {
      taxItem.taxPerDollar = -1
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when taxPerDollar is 1', function () {
      taxItem.taxPerDollar = 1
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return false when taxPerDollar is greater than 1', function() {
      taxItem.taxPerDollar = 1.1
      assert.equal(utils.isValidTaxTableItem(taxItem), false)
    })

    it('should return true when minSalary is 0', function() {
      taxItem.minSalary = 0
      assert.equal(utils.isValidTaxTableItem(taxItem), true)
    })

    it('should return true when maxSalary is 0', function() {
      taxItem.maxSalary = 0
      assert.equal(utils.isValidTaxTableItem(taxItem), true)
    })

    it('should return true when baseTax is 0', function() {
      taxItem.baseTax = 0
      assert.equal(utils.isValidTaxTableItem(taxItem), true)
    })

    it('should return true when taxPerDollar is 0', function() {
      taxItem.baseTax = 0
      assert.equal(utils.isValidTaxTableItem(taxItem), true)
    })

    it('should return true when object is valid tax table item', function() {
      assert.equal(utils.isValidTaxTableItem(taxItem), true)
    })
  })

  describe('.getTaxRate()', function() {
    var taxTable = null;

    beforeEach(function() {
      taxTable = [
        { minSalary: 0   , maxSalary: 100 },
        { minSalary: 101 , maxSalary: 200 },
        { minSalary: 201 , maxSalary: 300 },
        { minSalary: 351 , maxSalary: 371 },
        { minSalary: 401 , maxSalary: 0   }
      ]
    })


    it('should return tax free item when salary is in tax-free range', function() {
      var expected = { minSalary: 0, maxSalary: 100 }
      var actual   = utils.getTaxRate(taxTable, 50)
      assert.deepEqual(actual, expected)
    })

    it('should return tax free item when salary equals maxSalary of tax-free item', function() {
      var expected = { minSalary: 0, maxSalary: 100 }
      var actual   = utils.getTaxRate(taxTable, 100)
      assert.deepEqual(actual, expected)
    })

    it('should return expected item when salary is in one of item ranges', function() {
      var expected = { minSalary: 201, maxSalary: 300 }
      var actual   = utils.getTaxRate(taxTable, 210)
      assert.deepEqual(actual, expected)
    })

    it('should return expected item when salary equals minSalary of the item', function() {
      var expected = { minSalary: 201, maxSalary: 300 }
      var actual   = utils.getTaxRate(taxTable, 201)
      assert.deepEqual(actual, expected)
    })

    it('should return expected item when salary equals maxSalary of the item', function() {
      var expected = { minSalary: 201, maxSalary: 300 }
      var actual   = utils.getTaxRate(taxTable, 300)
      assert.deepEqual(actual, expected)
    })

    it('should return an item without max salary when salary is greater than maxSalary of any other item', function() {
      var expected = { minSalary: 401, maxSalary: 0}
      var actual   = utils.getTaxRate(taxTable, 420)
      assert.deepEqual(actual, expected)
    })

    it('should return null when salary does not match any range', function() {
      var expected = null;
      var actual   = utils.getTaxRate(taxTable, 380)
      assert.equal(actual, expected)
    })
  })
})
