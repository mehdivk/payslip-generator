#Payslip generator
Generates payslip based on Australia's tax rate table and employee's super annuation rate.

##Installation
```
npm install payslip-generator --save
```

##Running Tests
```
git clone git clone git@bitbucket.org:mehdivk/payslip-generator.git
cd payslip-generator
npm install
npm test
```
##Getting Started
`payslip-generator` exports a single class, PayslipGenerator.

```javascript
var PayslipGenerator = require('payslip-generator');
var payslipGenerator = new PayslipGenerator(taxTable);
```

**taxTable** is an array of taxTble items. Each item in taxTable must have following fields:
 - `minSalary`: A mandatory number (>= 0)
 - `maxSalary`: A mandatory number (>= 0)
 - `baseTax`: A mandatory number (>= 0)
 - `taxPerDollar`: A mandatory number (>= 0 & < 1)

An example of `taxTable` array:
```json
[
  {
    "minSalary": 0,
    "maxSalary": 18200,
    "baseTax": 0,
    "taxPerDollar": 0
  },
  {
    "minSalary": 18201,
    "maxSalary": 37000,
    "baseTax": 0,
    "taxPerDollar": 0.19
  },
  {
    "minSalary": 37001,
    "maxSalary": 80000,
    "baseTax": 3572,
    "taxPerDollar": 0.325
  },
  {
    "minSalary": 80001,
    "maxSalary": 180000,
    "baseTax": 17547,
    "taxPerDollar": 0.37
  },
  {
    "minSalary": 180001,
    "maxSalary": 0,
    "baseTax": 54547,
    "taxPerDollar": 0.45
  }
]
```

##PayslipGenerator.prototype.generate()
`.generate()` function expects following params:
 - `firstName`: mandatory string param
 - `lastName`: mandatory string param
 - `annualSalary`: mandatory positive number
 - `superRate`: mandatory positive number
 - `paymentPeriod`: A calendar month

```javascript
var PayslipGenerator = require('payslip-generator');
var taxTable         = require('./tax-table.json');
var payslip          = new PayslipGenerator(taxTable);
var result           = payslip.generate('Mehdi', 'Valikhani', 60500, 9, 'March');
```

`.generate()` function is a syncronous method, it may return an error or a payslip generator.
To make life easier we always return an array with maximum of two item.
 - `err`: An instance of `Error` class
 - `payslipObject`: Payslip object

###An example of payslip object
```json
{
  "firstName"      : "David",
  "lastName"       : "Rudd",
  "paymentPeriod"  : "March",
  "grossIncome"    : 5004,
  "incomeTax"      : 922,
  "netIncome"      : 4082,
  "superAnnuation" : 450
}
```
