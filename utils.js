function isValidTaxTableItem(item) {
  return isValidNumber(item.minSalary) && isValidNumber(item.maxSalary) &&
         isValidNumber(item.baseTax)   && isValidNumber(item.taxPerDollar) &&
         item.taxPerDollar < 1
}

function isValidNumber(value) {
  return typeof value == 'number' && value >= 0
}

function getTaxRate(taxTable, annualSalary) {
  var item = null;
  for (var index = 0; index < taxTable.length, item = taxTable[index]; index++) {
    if ( ( item.maxSalary == 0 || item.maxSalary >= annualSalary ) &&
        item.minSalary <= annualSalary) return item;
  }
  return null;
}

module.exports = {
  isValidTaxTableItem: isValidTaxTableItem,
  getTaxRate: getTaxRate,
  isValidNumber: isValidNumber
}
