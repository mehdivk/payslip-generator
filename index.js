var assert = require('assert')
var utils  = require('./utils')

function PayslipGenerator(taxTable) {
  assert.equal(arguments.length, 1, 'taxTable param is missing')
  assert.equal(taxTable.constructor === Array, true, 'taxTable should be an array')
  assert.equal(taxTable.length > 0, true, 'taxTable should have at least one item')
  assert.equal(taxTable.every(utils.isValidTaxTableItem), true, 'taxTable contains invalid items')

  this._taxTable = taxTable
}

//A syncronous method which generates a payslip object
//Returns [err, payslip]
//err: in case of error, it contains an error object
//payslip: in case of success, it contains payslip object for given data
function generate(firstName, lastName, annualSalary, superRate, paymentPeriod) {
  var config = utils.getTaxRate(this._taxTable, annualSalary)

  if (!config)          return [new Error('Could not find tax rates')]
  if (!firstName)       return [new Error('firstName is missing')]
  if (!lastName)        return [new Error('lastName is missing')]
  if (!annualSalary)    return [new Error('superRate is missing')]
  if (!paymentPeriod)   return [new Error('paymentPeriod is missing')]

  if (!utils.isValidNumber(superRate) || superRate >= 100) return [new Error('superRate is invalid')]
  if (!utils.isValidNumber(annualSalary) || annualSalary == 0) return [new Error('annualSalary is invalid')]

  var grossIncome    = Math.round(annualSalary / 12)
  var incomeTax      = Math.round((config.baseTax + ((annualSalary - config.minSalary - 1) * config.taxPerDollar)) / 12)
  var netIncome      = grossIncome - incomeTax
  var superAnnuation = Math.round((grossIncome * superRate) / 100)

  return [null, {
    firstName      : firstName,
    lastName       : lastName,
    paymentPeriod  : ['01', paymentPeriod, '-', '31', paymentPeriod].join(' '),
    grossIncome    : grossIncome,
    incomeTax      : incomeTax,
    netIncome      : netIncome,
    superAnnuation : superAnnuation
  }]
}


PayslipGenerator.prototype.generate = generate
module.exports = PayslipGenerator
